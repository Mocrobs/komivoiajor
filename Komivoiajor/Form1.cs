﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Komivoiajor
{
    public partial class Form1 : Form
    {
        List<City> listCitys;
        Bitmap bitmap;

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExit = new System.Windows.Forms.Button();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.viewMap = new System.Windows.Forms.PictureBox();
            this.groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewMap)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(280, 523);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 39);
            this.buttonExit.TabIndex = 1;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // groupBox
            // 
            this.groupBox.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox.Controls.Add(this.buttonStart);
            this.groupBox.Controls.Add(this.textBox1);
            this.groupBox.Controls.Add(this.buttonExit);
            this.groupBox.Location = new System.Drawing.Point(890, 0);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(400, 568);
            this.groupBox.TabIndex = 2;
            this.groupBox.TabStop = false;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(9, 533);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 3;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(9, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(377, 280);
            this.textBox1.TabIndex = 2;
            // 
            // viewMap
            // 
            this.viewMap.BackgroundImage = global::Komivoiajor.Properties.Resources.map;
            this.viewMap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.viewMap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewMap.ErrorImage = global::Komivoiajor.Properties.Resources.map;
            this.viewMap.InitialImage = global::Komivoiajor.Properties.Resources.map;
            this.viewMap.Location = new System.Drawing.Point(0, 0);
            this.viewMap.Name = "viewMap";
            this.viewMap.Size = new System.Drawing.Size(893, 568);
            this.viewMap.TabIndex = 0;
            this.viewMap.TabStop = false;
            this.viewMap.Click += new System.EventHandler(this.viewMap_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1288, 568);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.viewMap);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewMap)).EndInit();
            this.ResumeLayout(false);

        }
        public Form1()
        {
            InitializeComponent();
            viewMap.Width = this.Width / 3 * 2;
            viewMap.Height = this.Height;
            listCitys = new List<City>();
            viewMap.MouseClick += new MouseEventHandler((obj, arg) => {
                listCitys.Add(new City(arg.X, arg.Y, listCitys.Count));                
                fillCitys();                
            });
        }

        private void Form1_Load(object sender, EventArgs e)
        {           
            viewMap.Width = this.Width / 3 * 2;
            viewMap.Height = this.Height;
            groupBox.Location = new System.Drawing.Point(viewMap.Width, 0);
            groupBox.Width = this.Width / 3;
            groupBox.Height = this.Height;
            buttonExit.Location = new System.Drawing.Point(groupBox.Width - buttonExit.Width - 10, groupBox.Height - buttonExit.Height - 10);
            fillCitys();

        }       

        public void fillCitys()
        {
            Bitmap canvas = new Bitmap(Properties.Resources.map);
            bitmap = new Bitmap(canvas.Width, canvas.Height);
            Graphics graph = Graphics.FromImage(bitmap);
            SolidBrush brush = new SolidBrush(Color.Red);
            viewMap.Height = this.Height;
            foreach (City city in listCitys) {
                graph.FillEllipse(brush, city.x, city.y, 10, 10);
            }
            viewMap.Image = bitmap;

        }

        public void DrawPath(List<City> path)
        {
           
            Graphics graph = Graphics.FromImage(bitmap);
            Pen pen = new Pen(Color.Black, 2);
           
            viewMap.Height = this.Height;
            for(int i =1; i<path.Count;i++)
            {
                graph.DrawLine(pen, path[i-1].x + 5, path[i - 1].y + 5, path[i].x + 5, path[i].y + 5);
            }
            graph.DrawLine(pen, path[path.Count - 1].x + 5, path[path.Count - 1].y + 5, path[0].x + 5, path[0].y + 5);
            viewMap.Image = bitmap;

        }

        #region Код, автоматически созданный конструктором форм Windows
        #endregion

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            DMatrix res = Utils.ToAdjacency(listCitys);

            for (int i = 0; i < res.NbRows; i++)
            {
                for (int j = 0; j < res.NbCols; j++)
                {
                    textBox1.Text += Math.Round(res[j, i],1) + "\t";
                }
                textBox1.Text += Environment.NewLine;
            }
            textBox1.Text += Environment.NewLine;
            List<City> path = Utils.FrontBranch(res, listCitys);
            foreach(City city in path)
            {
                textBox1.Text += city.index + "\t";
            }
            DrawPath(path);
        }

        private void viewMap_Click(object sender, EventArgs e)
        {

        }
    }
}
